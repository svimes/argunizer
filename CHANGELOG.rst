=========
CHANGELOG
=========

0.1.4 - 2020-04-13
------------------

Fixed
^^^^^

 - Fix packaging.

0.1.3 - 2020-04-13
------------------

Fixed
^^^^^

- Better error handling for directories, which contains broken symlinks.


0.1.2 - 2020-04-13
------------------

Fixed
^^^^^

- Documentation and packaging related fixes.
 

0.1.1 - 2020-04-13
------------------

Fixed
^^^^^

- Packaging and documentation related fixes.

0.1.0 - 2020-04-13
------------------

Changed
^^^^^^^

- Initial release, which includes:
  - base Argunizer class, which implements the timeline creation and maintenance
  - scripts, which drive the symlink, copy, move and clean semantics.
