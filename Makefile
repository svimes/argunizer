init:
	pip install -r requirements.txt
test:
	py.test tests
clean:
	rm -rf argunizer_pkg_svimes.egg-info
	rm -rf build/*
	rm -f dist/*
	rm -rf argunizer/__pycache__

.PHONY: init test clean
