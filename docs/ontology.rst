Ontology
=====

argumenter
  Argumenter sets up a given timeline creation runtime parameters either through the command line arguments or a YAML configuration file.

cleaner
  Cleaner tidies up the source directory structure after the mover has exhausted its activities.

copier
  Copier copies a file in place in the temporal timeline.

destination directory
  Destination directory is a file path under which the timeline is constructed.

executor
  Executor takes a modification time and timeline as an argument and checks / modifies the destination directory for the necessary elements in the timeline.

mover
  Mover moves a file in place in the temporal timeline. 

shaker
  Shaker defines the semantics of the temporal displacement retrieved from the argumenter, i.e. it instructs the walker and director and passes on the instructions to the mover, copier and cleaner as appropriate.

source directory
  Source directory is a file path, under which a number of source files reside.

walker 
  Walker walks a base directory hierarchy and turns it into a list of file paths, names and last modification times.
